CREATE DATABASE firsrapi;
\l
\c firsrApi

CREATE TABLE users(
    id SERIAL PRIMARY KEY,
    name VARCHAR(40),
    email TEXT
);

INSERT INTO users(name, email) VALUES ('Juan', 'juan@masync.com'),('Daniela', 'daniela@masync.com');
