const express = require('express');
require('dotenv').config({ path: 'src/.env' });
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use(require('./routes/index'));
app.listen(process.env.PORT, ()=>{
    console.log(`PORT IN ${process.env.PORT}`);
    
})
