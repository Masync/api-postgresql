const express = require('express');
const { getUsers, insertUsers, getUserById,deleteUsers,updateUsers } = require('../controllers/index.controller');
const routes = express.Router();

routes.get('/users',getUsers );
routes.get('/users/:id',getUserById );
routes.post('/users',insertUsers );
routes.put('/users/:id', updateUsers);
routes.delete('/users/:id', deleteUsers);


module.exports = routes;