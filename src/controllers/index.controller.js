const {Pool} = require('pg');
require('dotenv').config({ path: 'src/.env' });

const pool = new Pool({
    host: process.env.HOST,
    user: process.env.USERDB,
    password: '',
    database: process.env.DB,
    port: process.env.PORTDB
});
const  getUsers = async (req, res)=>{
   const resp = await pool.query('SELECT  id,name,email FROM users');
    res.status(200).json(resp.rows);
};

const getUserById = async (req, res) =>{
    const id = req.params.id;
    const resp = await pool.query('SELECT id, name, email FROM users WHERE id = $1', [id]);
    res.status(200).json(resp.rows);
};

const insertUsers = async (req, res) =>{
    const { name, email } = req.body;

    const resp = await pool.query('INSERT INTO users (name, email) VALUES ($1, $2)', [name, email]);
    console.log(resp);
    res.json({
        message: 'User add succefully',
        body: {
            user: {name, email}
        }
    }).status(200);
    
};

const deleteUsers = async (req, res)=>{
    const id = req.params.id;
    const resp = await pool.query('DELETE FROM users WHERE id = $1', [id]);
    res.json({
        message: 'User deleted succefully',
        body: {
            user: {name, email}
        }
    }).status(200);
};

const updateUsers = async (req, res)=>{
    const id = req.params.id;
    const { name, email} = req.body;
    const resp = await pool.query('UPDATE users SET  name =$1, email = $2 WHERE id = $3', [name,email,id]);
    res.json({
        message: 'User Updated succefully',
        body: {
            user: {name, email}
        }
    }).status(200);
}
module.exports = {
    getUsers,
    getUserById,
    insertUsers,
    deleteUsers,
    updateUsers
    
};